FROM python:3.6-slim
RUN apt-get clean
RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev build-essential nginx
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["app.py"]
